#!/usr/bin/env python3 


import hashlib

username=b"FRASER"

order=[4,5,3,6,2,7,1,8]

key='picoCTF{1n_7h3_|<3y_of_'
x=hashlib.sha256(username).hexdigest()
for i in order:
	key+=x[i]

key+='}'
print(key)
