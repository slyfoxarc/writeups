#!/usr/bin/env python3

import requests


header={
	"user-agent":"PicoBrowser",
	"Host":"mercury.picoctf.net:46199",
	"Origin":"http://mercury.picoctf.net:46199",
	"Referer":"http://mercury.picoctf.net:46199",
	"Date":"Sun,01 Jan 2018",
	"DNT":"1",
	"Accept-Language":"sv",
	"X-Forwarded-For":"2.16.174.0"

}

r=requests.get("http://mercury.picoctf.net:46199/",headers=header)

ind=r.text.find("picoCTF{")

flag=r.text[ind:ind+49]
print(flag)
