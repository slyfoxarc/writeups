#!/usr/bin/env python3 


alphabet = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ"+ \
            "[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"

text="A:4@r%uL`M-^M0c0AbcM-MFE07b34c`_6N"
const=47
flag=""


for i in text:
	ind=alphabet.find(i)
	ind-=47
	if ind < 0 :
		ind+=len(alphabet)
	flag+=alphabet[ind]


print(flag)